# Setup Database

Connect to AppTier EC2 Instance from the AWS Console. Instance > Connect > Session Manager

Switch to ec2-user
```
sudo -su ec2-user
cd ~
```


Initiate your DB connection with your Aurora RDS writer endpoint. In the following command, replace the RDS writer endpoint and the username, and then execute it in the browser terminal:

```
mysql -h CHANGE-TO-YOUR-RDS-ENDPOINT -u admin -p
```

You will then be prompted to type in your password. Once you input the password and hit enter, you should now be connected to your database.

NOTE: If you cannot reach your database, check your credentials and security groups.


You can verify that it was created correctly with the following command:

```
SHOW DATABASES;
```

Create a data table by first navigating to the database we just created:

```
USE webappdb; 
```   

Then, create the following transactions table by executing this create table command:


```
CREATE TABLE IF NOT EXISTS transactions(id INT NOT NULL
AUTO_INCREMENT, amount DECIMAL(10,2), description
VARCHAR(100), PRIMARY KEY(id)); 
```   

Verify the table was created:

```
SHOW TABLES; 
```   

Insert data into table for use/testing later:

```
INSERT INTO transactions (amount,description) VALUES ('400','groceries');  
``` 

Verify that your data was added by executing the following command:

```
SELECT * FROM transactions;
```

When finished, just type exit and hit enter to exit the MySQL client.

# Setup Backend (NodeJS App)

Update the DB_HOST value in app-tier/DbConfig.js with the endpoint value from terraform output

Test AppTier connection with database by running below command

```
curl http://localhost:4000/transaction
```