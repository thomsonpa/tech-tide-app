#########################
### Load balancer DNS ###
#########################

output "public_lb_dns_name" {
  description = "DNS name of the load balancer"
  value       = aws_lb.application-load-balancer.dns_name
}

output "internal_lb_dns_name" {
  description = "DNS name of the internal load balancer"
  value       = aws_lb.internal-load-balancer.dns_name
}

output "rds_endpoint" {
  description = "RDS Endpoint"
  value       = aws_db_instance.database-instance.endpoint
}


# output "app_instance_ip" {
#   description = "Private IP Address of the App Tier Instance"
#   value       = aws_instance.private-app-template.private_ip
# }
